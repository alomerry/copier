module github.com/alomerry/copier

go 1.13

require (
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.6.1
	golang.org/x/net v0.1.0
)
